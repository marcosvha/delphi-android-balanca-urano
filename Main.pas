{*
 * LePeso Android USB Serial - Delphi 10
 * Baseado na Demo da Winsoft para o componente TUsbSerial
 *
 * 2019 Marcos Vinicius Henke Arnoldo
 * contato: b4it.b4sig@gmail.com
 *
 * Apoio: Fabio Bica - Balan�as Urano
 *
 *}
unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.ScrollBox,
  FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls, Winsoft.Android.UsbSer,
  Androidapi.JNIBridge, Winsoft.Android.Usb, FMX.ListBox, FMX.Edit,
  System.StrUtils;

type
  TFormMain = class(TForm)
    ButtonOpenClose: TButton;
    Memo: TMemo;
    Timer: TTimer;
    ComboBoxDevices: TComboBox;
    CheckBoxSynchronous: TCheckBox;
    TimerPermission: TTimer;
    Button1: TButton;
    Button2: TButton;
    edtPreco: TEdit;
    btnTeste50repeticoes: TButton;
    procedure ButtonOpenCloseClick(Sender: TObject);
    procedure MemoChangeTracking(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerPermissionTimer(Sender: TObject);
    procedure ComboBoxDevicesChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnTeste50repeticoesClick(Sender: TObject);
  private
    { Private declarations }
    UsbDevices: TArray<JUsbDevice>;
    UsbSerial: TUsbSerial;
    Text: string;
    procedure OnBreakInterrupt;
    procedure OnCtsChanged(State: Boolean);
    procedure OnDsrChanged(State: Boolean);
    procedure OnFramingError;
    procedure OnOverrunError;
    procedure OnParityError;
    procedure OnReceivedData(Data: TJavaArray<Byte>);
    procedure OnDeviceAttached(Device: JUsbDevice);
    procedure OnDeviceDetached(Device: JUsbDevice);
    procedure AppendText(const NewText: string);
    procedure RefreshDevices;
    procedure Open;
    procedure Close;
    procedure GravaPreco(PsPrecoSomenteDigitos: string);
    procedure LerPeso;
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses Androidapi.Jni.App, Androidapi.Jni.JavaTypes, Androidapi.Helpers,
  Androidapi.Jni.Widget, Androidapi.Jni.Os, FMX.Helpers.Android, Androidapi.Jni;

{$R *.fmx}

function AndroidApi: Integer;
begin
  Result := TJBuild_VERSION.JavaClass.SDK_INT;
end;

procedure ShowInfo(const Message: string);
begin
  CallInUIThread(
    procedure
    begin
      TJToast.JavaClass.makeText(TAndroidHelper.Context,
        StrToJCharSequence(Message), TJToast.JavaClass.LENGTH_LONG).show;
    end);
end;

function ByteArrayToString(Data: TJavaArray<Byte>): string;
begin
  Result := '';
  try
    if (Data <> nil) and (Data.Length > 0) then
      Result := TEncoding.ANSI.GetString(ToByteArray(Data));
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TFormMain.OnBreakInterrupt;
begin
  ShowInfo('Break interrupt');
end;

procedure TFormMain.OnCtsChanged(State: Boolean);
begin
  if State then
    ShowInfo('CTS ON')
  else
    ShowInfo('CTS OFF');
end;

procedure TFormMain.OnDsrChanged(State: Boolean);
begin
  if State then
    ShowInfo('DSR ON')
  else
    ShowInfo('DSR OFF');
end;

procedure TFormMain.OnFramingError;
begin
  ShowInfo('Framing error');
end;

procedure TFormMain.OnOverrunError;
begin
  ShowInfo('Overrun error');
end;

procedure TFormMain.OnParityError;
begin
  ShowInfo('Parity error');
end;

procedure TFormMain.OnReceivedData(Data: TJavaArray<Byte>);
begin
  CallInUIThread(
    procedure
    begin
      AppendText(ByteArrayToString(Data));
      //ShowInfo('ReceivedData: ' + ByteArrayToString(Data));
    end);
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  UsbSerial := TUsbSerial.Create;
  UsbSerial.OnBreakInterrupt := OnBreakInterrupt;
  UsbSerial.OnCtsChanged := OnCtsChanged;
  UsbSerial.OnDsrChanged := OnDsrChanged;
  UsbSerial.OnFramingError := OnFramingError;
  UsbSerial.OnOverrunError := OnOverrunError;
  UsbSerial.OnParityError := OnParityError;
  UsbSerial.OnReceivedData := OnReceivedData;
  UsbSerial.OnDeviceAttached := OnDeviceAttached;
  UsbSerial.OnDeviceDetached := OnDeviceDetached;
  RefreshDevices;
end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
  UsbSerial.Free;
end;

procedure TFormMain.RefreshDevices;
var
  I: Integer;
  Device: JUsbDevice;
begin
  ComboBoxDevices.Clear;
  UsbDevices := UsbSerial.UsbDevices;
  if UsbDevices <> nil then
    for I := 0 to Length(UsbDevices) - 1 do
    begin
      Device := UsbDevices[I];
      if AndroidApi >= 21 then
        ComboBoxDevices.Items.Add(JStringToString(Device.getManufacturerName) + ' ' + JStringToString(Device.getProductName))
      else
        ComboBoxDevices.Items.Add(JStringToString(Device.getDeviceName));
      if Device = UsbSerial.UsbDevice then
        ComboBoxDevices.ItemIndex := I;
    end;

  if ComboBoxDevices.ItemIndex = -1 then
  begin
    Close;
    if ComboBoxDevices.Items.Count > 0 then
      ComboBoxDevices.ItemIndex := 0;
  end;

  ComboBoxDevicesChange(Self);
end;

procedure TFormMain.ComboBoxDevicesChange(Sender: TObject);
begin
  ButtonOpenClose.Enabled := ComboBoxDevices.ItemIndex <> -1;
end;

procedure TFormMain.OnDeviceAttached(Device: JUsbDevice);
begin
  RefreshDevices;
end;

procedure TFormMain.OnDeviceDetached(Device: JUsbDevice);
begin
  RefreshDevices;
end;

procedure TFormMain.Open;
var Device: JUsbDevice;
begin
  TimerPermission.Enabled := False;
  //Memo.Lines.Clear;
  Text := Memo.Text;
  Memo.SetFocus;
  Memo.GoToTextEnd;
  Memo.SelStart := Length(Text);
  Memo.SelLength := 0;

  Device := UsbDevices[ComboBoxDevices.ItemIndex];
  if not UsbSerial.IsSupported(Device) then
    raise Exception.Create('Unsupported device');

  if not UsbSerial.HasPermission(Device) then
  begin
    UsbSerial.RequestPermission(Device);
    if not UsbSerial.HasPermission(Device) then
    begin
      TimerPermission.Enabled := True;
      Exit;
    end;
  end;

  UsbSerial.Connect(Device);
  UsbSerial.Open(CheckBoxSynchronous.IsChecked);
  ComboBoxDevices.Enabled := False;
  CheckBoxSynchronous.Enabled := False;
  ButtonOpenClose.Text := 'Close';
  Memo.Enabled := True;

  if UsbSerial.Synchronous then
    Timer.Enabled := True;
end;

procedure TFormMain.Close;
begin
  Timer.Enabled := False;
  TimerPermission.Enabled := False;
  UsbSerial.Close;
  UsbSerial.Disconnect;
  ComboBoxDevices.Enabled := True;
  CheckBoxSynchronous.Enabled := True;
  ButtonOpenClose.Text := 'Open';
  //Memo.Enabled := False;
end;

procedure TFormMain.btnTeste50repeticoesClick(Sender: TObject);
var
  I: integer;
begin
  for I:=1 to 50 do
  begin
    CallInUIThread(
    procedure
    begin
      Sleep(300);
      AppendText(Format('Teste %d de 50:'#13#10, [I]));
    end);

    CheckBoxSynchronous.IsChecked := True; // Assincrono trava
    Open();
    GravaPreco(Format('%6.6d', [ I * 100 ]));
    Sleep(300);
    LerPeso(); // Obs.: N�o consegui exibir o retorno do le peso quando processado em lote
    Sleep(300);
    Close();
{
    CallInUIThread(
    procedure
    begin
      Sleep(500);
    end);
}
  end;
end;

procedure TFormMain.GravaPreco(PsPrecoSomenteDigitos: string);
var
  Buffer: TArray<Byte>;
  sPreco, sChecksum, sFrame: string;
  cCS1, cCS2: char;
  I: integer;
  iSoma: integer;
begin
  try
    sPreco := Format('%6.6d', [StrToInt(Trim(PsPrecoSomenteDigitos))]);
    iSoma := 0;
    // Bomba: strings para mobile tem o primeiro char com indice 0!!! (Para desktop � 1, mesmo no D10)
    // http://docwiki.embarcadero.com/RADStudio/Rio/en/Migrating_Delphi_Code_to_Mobile_from_Desktop
    for I := 0 to 5 do
      Inc(iSoma, Ord(sPreco[I]));

    sChecksum := IntToHex(iSoma, 4); //0133
    AppendText(sChecksum);

    cCS1 := Chr(StrToInt('$' + Copy(sChecksum, 1, 2)));
    cCS2 := Chr(StrToInt('$' + Copy(sChecksum, 3, 2)));

    sFrame := #7 + sPreco + cCS1 + cCS2 + #13#10;
    AppendText(sFrame);

    SetLength(Buffer, 11);
    Buffer[0] := $07; // Bell (bip)
    for I:= 1 to 6 do
      Buffer[I] := Ord(sPreco[I-1]); // Cuidado com o indice do caractere inicial. Mobile � 0!!!

    Buffer[7] := Ord(cCS1);
    Buffer[8] := Ord(cCS2);

    Buffer[9]  := $0D; //13   CR
    Buffer[10] := $0A; //10   LF

    UsbSerial.Write(Buffer, 300);

    AppendText(TEncoding.ANSI.GetString(Buffer));

  except
    On E:Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TFormMain.Button1Click(Sender: TObject);
begin
  GravaPreco(edtPreco.Text);
end;

procedure TFormMain.Button2Click(Sender: TObject);
begin
  LerPeso();
end;

procedure TFormMain.LerPeso();
var
  Buffer: TArray<Byte>;
begin
// Para ler o peso e total
{
				byte [] buf = new byte [2];
				    buf[0] = 0x04;
				    mSerial.write(buf);
}
  try
    SetLength(Buffer, 2);
    Buffer[0] := $04;
    UsbSerial.Write(Buffer, 300);
  except
    On E:Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TFormMain.ButtonOpenCloseClick(Sender: TObject);
begin
  if not UsbSerial.Opened then
  begin
    Memo.Lines.Clear;
    Open;
  end
  else
    Close
end;

procedure TFormMain.MemoChangeTracking(Sender: TObject);
var
  MemoText: string;
  KeyChar: Char;
  Found: Boolean;
  I: Integer;
begin
{
  // Memo OnKeyDown doesn't work, so we need to track changes manually
  if UsbSerial.Opened then
  begin
    MemoText := Memo.Text;
    if MemoText <> Text then
    begin
      if Length(MemoText) > Length(Text) then // character was added
      begin
        KeyChar := #0;
        Found := False;
        for I := 0 to Length(Text) - 1 do
          if MemoText[I] <> Text[I] then
          begin
            KeyChar := MemoText[I]; // first different character found
            Found := True;
            Break;
          end;

        if not Found then
          KeyChar := MemoText[Length(Text)];

        UsbSerial.Write(Ord(KeyChar), 300);
      end;
      Text := MemoText;
    end;
  end;
}
end;

procedure TFormMain.TimerPermissionTimer(Sender: TObject);
var Device: JUsbDevice;
begin
  try
    Device := UsbDevices[ComboBoxDevices.ItemIndex];
    if UsbSerial.HasPermission(Device) then
      Open;
  except
    On E:Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TFormMain.TimerTimer(Sender: TObject);
var
  Buffer: TArray<Byte>;
  Size: Integer;
begin
  try
    SetLength(Buffer, 55);
    Size := UsbSerial.Read(Buffer, 1);
    if Size >= 1 then
    begin
      SetLength(Buffer, Size);
      AppendText(TEncoding.ANSI.GetString(Buffer));
    end;
  except
    On E:Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TFormMain.AppendText(const NewText: string);
begin
  try
    if NewText = '' then
      Exit;

    Text := Memo.Text + NewText;
    Memo.Text := Text;
    Memo.GoToTextEnd;
    Memo.SelStart := Length(Text);
    Memo.SelLength := 0;
  except
    On E:Exception do
      ShowMessage(E.Message);
  end;
end;

end.
