# Delphi Android Balança Urano
### Desenvolvido em Delphi 10.2 Tokyo Community Edition 

Exemplo de como ler peso e setar preço de balanças Urano Pop Z através de um aplicativo para Android!

Contém o cálculo do checksum necessário para setar o preço!  


## Requisitos:
- Cabo USB específico: https://www.urano.com.br/cabo-adaptador-serial-urano-ttl-usb.html   
- Driver CP210x USB to UART Bridge Virtual COM Port (VCP): https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers  
- Componente Winsoft ComPort for Android USB Serial: https://www.winsoft.sk/acpusbser.htm

